import React from 'react';
import { Container, Row, Col, Card, CardBody, Form, FormGroup, Label, Input, Button } from 'reactstrap';
import axios from 'axios';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { updatelist } from '../../actions/postActions';
import Spinner from './../spinner/spinner';

class EditPostForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            postid: '',
            title: '',
            description: '',
            error: {}
        };
        this.getSinglePost = this.getSinglePost.bind(this);
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if (JSON.stringify(nextProps.post.error) !== JSON.stringify(prevState.error)) {
            return {
                error: nextProps.post.error
            };
        }
        return null;
    }

    changeHandler = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    submitHandler = (event) => {
        event.preventDefault()
        this.props.actions.updatelist(this.state.postid, this.state.title, this.state.description);
    }

    componentDidMount() {
        const { match: { params } } = this.props;
        this.getSinglePost(params.id);
    }

    getSinglePost(id) {
        var token = localStorage.getItem("token");
        let dis = this;
        return axios.get('http://localhost:8000/api/posts/' + id + '?token=' + token,
            {
                headers: {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": "*"
                }
            })
            .then(function (response) {
                dis.setState({ postid: response.data.data.id, title: response.data.data.title, description: response.data.data.description, loading: false });
            })
            .catch(function (error) {
                dis.setState({ loading: true });
                console.log(error);
            })
    }

    render() {
        let { title, description, error } = this.state
        if (this.state.loading) {
            return <div>
                <Spinner />
            </div>
        }
        return (
            <div className="posts-wall">
                <Container>
                    <Row>
                        <Col sm="12" md="12">
                            <div className="posts-wall-heading">
                                <h2>Update Post</h2>
                                <div className="st-border"></div>
                            </div>
                        </Col>
                        <Col sm="12" md="12">
                            <Card className="post-card">
                                <CardBody>
                                    <Form onSubmit={this.submitHandler}>
                                        <FormGroup>
                                            <Label>Post Title</Label>
                                            <Input
                                                type="text"
                                                name="title"
                                                id="title"
                                                className={
                                                    error.title ? "is-invalid" : ""
                                                }
                                                placeholder="Type Your Post Title Here"
                                                value={title}
                                                onChange={this.changeHandler}
                                            />
                                            {error.title ? <div className="invalid-feedback">{error.title}</div> : ''}
                                        </FormGroup>
                                        <FormGroup>
                                            <Label>Post Description</Label>
                                            <Input
                                                type="textarea"
                                                name="description"
                                                id="description"
                                                className={
                                                    error.description ? "is-invalid" : ""
                                                }
                                                placeholder="Type Your Post Description Here"
                                                value={description}
                                                onChange={this.changeHandler}
                                            />
                                            {error.description ? <div className="invalid-feedback">{error.description}</div> : ''}
                                        </FormGroup>
                                        <FormGroup>
                                            <Button
                                                type="submit"
                                                color="primary"
                                            >
                                                Create
                                            </Button>
                                        </FormGroup>
                                    </Form>
                                </CardBody>
                            </Card>
                        </Col>
                    </Row>
                </Container>
            </div>
        )
    }
}


function mapStateToProps(state) {
    return {
        post: state.post
    }
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators({ updatelist }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditPostForm);