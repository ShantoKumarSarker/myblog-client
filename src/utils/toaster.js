import { toast } from 'react-toastify'

export const toastify = (type, msg) => {
    if(type==='success'){
        return toast.success(msg, {
            position: toast.POSITION.TOP_RIGHT
            });
    } else if (type==='error') {
        return toast.error(msg, {
            position: toast.POSITION.TOP_RIGHT
          });
    } else if (type==='info') {
        return toast.info(msg, {
            position: toast.POSITION.TOP_RIGHT
          });
    } else {
        return ''
        //do nothing
    }
    
}