import React from 'react';
import {
    Route,
    Redirect
} from "react-router";
import { isAuthenticated } from './../../utils/auth';

const AuthRoute = ({ component: Component, ...rest }) => (
    <Route
        {...rest}
        render={(props) => isAuthenticated() ? (
            <Component {...props} />
        ) : (
                <Redirect
                    to={{
                        pathname: "/signin",
                        state: { from: props.location }
                    }}
                />
            )
        }
    />
);

