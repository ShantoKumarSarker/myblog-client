import React from 'react';
import { Link } from 'react-router-dom';
import {
  Container,
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  Badge,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from 'reactstrap';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { logout_success, logout } from '../../actions/loginActions';
import { notificationArrived } from '../../actions/notificationActions';

class Header extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this)
    this.state = {
      isOpen: false,
      notification: [],
      notificationCount: 0
    }
    // this.takeNotification = this.takeNotification.bind(this)
  }

  componentDidMount() {
    this.props.actions.notificationArrived()
    // this.takeNotification()
  }

  // takeNotification(){
  //   setTimeout(() => this.setState({ notification: this.props.notification }), 3000);
  //   //setInterval(() => console.log(this.state.notification), 5000);
  // }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  clickHandler = (e) => {
    e.preventDefault();
    this.props.actions.logout({});
  }

  render() {
    return (
      <div>
        <header className="home-header">
          <Navbar className="main-nav" color="light" light expand="lg">
            <Container>
              <NavbarBrand className="text-dark">
                <span className="font-weight-bold">MyBlog</span>
              </NavbarBrand>
              <NavbarToggler onClick={this.toggle} />
              <Collapse isOpen={this.state.isOpen} navbar>
                <Nav className="ml-auto" navbar>
                  <NavItem>
                    <Link className="nav-link" to="/create-post">Create New Post</Link>
                  </NavItem>
                  <NavItem>
                    <Link className="nav-link" to="/home">All Post</Link>
                  </NavItem>
                  <UncontrolledDropdown nav inNavbar>
                    <DropdownToggle nav caret>
                      <i className="fas fa-bell"></i>
                      {this.props.notification.length ? 
                        <Badge color="danger">{this.props.notification.length}</Badge> : <div></div>
                      }                      
                    </DropdownToggle>
                    <DropdownMenu right>
                      {this.props.notification ? this.props.notification.map((msg, index) => {
                        return (
                          <div key={index}>
                            <DropdownItem> {msg} </DropdownItem>
                            <DropdownItem divider />
                          </div>
                        )
                      }) : <DropdownItem> Nothing New Here </DropdownItem>}
                    </DropdownMenu>
                  </UncontrolledDropdown>
                  <UncontrolledDropdown nav inNavbar>
                    <DropdownToggle nav caret>
                      Account <i className="fas fa-chevron-down"></i>
                    </DropdownToggle>
                    <DropdownMenu right>
                      <DropdownItem onClick={this.clickHandler}>
                        Sign Out
                      </DropdownItem>
                    </DropdownMenu>
                  </UncontrolledDropdown>
                </Nav>
              </Collapse>
            </Container>
          </Navbar>
        </header>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    logout: state.logout,
    notification: state.notification
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ logout_success, logout, notificationArrived }, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);