import * as types from '../actions/actionTypes';

const initialState = {
    loading: true,
    error: {}
};

export default function registerReducer(state = initialState, action) {
    switch (action.type) {
        case types.REGISTER_ERROR:
            return { ...state, loading: false, error: action.payload };
        case types.REGISTER_SUCCESS:
            return { ...state, loading: false, error: {} };
        default:
            return state;
    }
}
