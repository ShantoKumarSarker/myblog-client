import io from 'socket.io-client';

import * as actionTypes from './actionTypes';
import { toastify } from '../utils/toaster';

var socket = io('http://localhost:5000');


export function broadcast (msg) {
    socket.emit("notification", {msg: msg});
}

export function new_notification(data) {
    return {
        type: actionTypes.NEW_NOTIFICATION,
        payload: data
    }
}

export function view_notification() {
    return {
        type: actionTypes.VIEW_NOTIFICATION
    }
}

export function notificationArrived() {
    return (dispatch) => {
        socket.on('notification', (data) => {
            // console.log(data);
            dispatch(new_notification(data))
            toastify('info', 'New Post Arrived !!');
        });
    }
}

export function notificationViewed() {
    return (dispatch) => {
        dispatch(view_notification())
    }
}