import React, { Component } from 'react';
import { Provider } from "react-redux";
import { Router, Route, Redirect } from 'react-router-dom';
import { history, store } from './store';
import { isAuthenticated } from './utils/auth';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Spinner from './components/spinner/spinner';
import Home from './components/pages/home';
import CreatePost from './components/pages/createpost';
import EditPost from './components/pages/editpost';
import Signin from './components/pages/login';
import Signup from './components/pages/register';


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true
    }
  }

  componentDidMount() {
    setTimeout(() => this.setState({ loading: false }), 500);
  }

  render() {
    const { loading } = this.state;
    return (
      <Provider store={store}>
        <ToastContainer autoClose={5000}/>
        {loading ?      
        <Spinner/>
        :
        <Router history={history}>
          <div>
            <Route exact path='/' component={Signin} />
            <AuthRoute exact path='/home' component={Home} />
            <AuthRoute exact path='/create-post' component={CreatePost} />
            <AuthRoute exact path='/edit-post/:id' component={EditPost} />
            <Route exact path='/signin' component={Signin} />
            <Route exact path='/signup' component={Signup} />
          </div>
        </Router>}
      </Provider>
    );

    function AuthRoute ({ component: Component, ...rest }) {
      return (
        <Route
            {...rest}
            render={(props) => isAuthenticated() ? (
                <Component {...props} />
            ) : (
                    <Redirect
                        to={{
                            pathname: "/signin",
                            state: { from: props.location }
                        }}
                    />
                )
            }
        />
      )
    }  
  }
}

export default App;
