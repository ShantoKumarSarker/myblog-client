import React from 'react';
import { ClipLoader } from 'react-spinners';
import './spinner.css';
 
export default class Spinner extends React.Component {
  render() {
    return (
      <div className='sweet-loading'>
        <ClipLoader
            color={'#4CAF50'}
            loading={this.props.loading} 
        />
      </div>
    )
  }
}