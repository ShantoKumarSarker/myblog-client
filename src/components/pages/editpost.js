import React from 'react';
import Helmet from 'react-helmet';
import Header from './../header/navigation';
import Footer from './../footer/footer';
import EditPostForm from './../post/edit';

export default class EditPost extends React.Component {
    render() {
        return (
            <div>
                <Helmet>
                    <title>Create Post | MyBlog</title>
                    <meta name="description" content="Create Post | MyBlog" />
                </Helmet>
                <div>
                    <Header />
                    <EditPostForm {...this.props}/>
                    <Footer />
                </div>
            </div>
        )
    }
}