import React from 'react';
import Helmet from 'react-helmet';
import { Link } from 'react-router-dom';
import { Container, Row, Col, Form, FormGroup, Label, Input, Button } from 'reactstrap';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { login } from '../../actions/loginActions';

class Signin extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            loggedIn: false,
            email: '',
            password: '',
            error: {}
        }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if (JSON.stringify(nextProps.auth.error) !== JSON.stringify(prevState.error)) {
            return {
                error: nextProps.auth.error
            };
        }
        return null;
    }

    changeHandler = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    submitHandler = (event) => {
        event.preventDefault()
        this.props.actions.login(this.state.email, this.state.password);
    }

    render() {
        let { email, password, error } = this.state
        return (
            <div>
                <Helmet>
                    <title>Sign In | MyBlog</title>
                    <meta name="description" content="Sign In | Myblog" />
                </Helmet>
                <div className='signin'>
                    <Container>
                        <Row>
                            <Col sm="12" md={{ size: 6, offset: 3 }}>
                                <Form className="signin-form" onSubmit={this.submitHandler}>
                                    <div className="form-heading">
                                        <h2>Sign In @ MyBlog</h2>
                                        <div className="st-border"></div>
                                    </div>
                                    <FormGroup>
                                        <Label>Your Email</Label>
                                        <Input
                                            type="email"
                                            name="email"
                                            id="email"
                                            className={
                                                error.email ? "is-invalid" : ""
                                            }
                                            value={email}
                                            placeholder="Type Your Email Here"
                                            onChange={this.changeHandler}
                                        />
                                        {error.email ? <div className="invalid-feedback">{error.email}</div> : ''}
                                    </FormGroup>
                                    <FormGroup>
                                        <Label>Your Password</Label>
                                        <Input
                                            type="password"
                                            name="password"
                                            id="password"
                                            className={
                                                error.password ? "is-invalid" : ""
                                            }
                                            value={password}
                                            placeholder="Type Your Password Here"
                                            onChange={this.changeHandler}
                                        />
                                        {error.password ? <div className="invalid-feedback">{error.password}</div> : ''}
                                    </FormGroup>
                                    <FormGroup>
                                        <Link className="form-link" to="/signup">Don't have any account yet? Sign Up</Link>
                                    </FormGroup>
                                    <FormGroup>
                                        <Button
                                            type="submit"
                                            color="success"
                                            className="btn-block"
                                        >
                                            Login
                                        </Button>
                                    </FormGroup>
                                </Form>
                            </Col>
                        </Row>
                    </Container>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        auth: state.auth
    }
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators({ login }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Signin);