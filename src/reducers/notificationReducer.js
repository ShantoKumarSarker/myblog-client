import * as types from '../actions/actionTypes';

const initialState = [];

export default function notificationReducer(state = initialState, action) {
    switch (action.type) {
        case types.NEW_NOTIFICATION:
            state.push(action.payload.msg);
            return state.slice(0);
        case types.VIEW_NOTIFICATION:
            return [];
        default:
            return state;
    }
}
