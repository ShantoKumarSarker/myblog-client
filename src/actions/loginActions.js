import * as actionTypes from './actionTypes';
import { history } from './../store';
import axios from 'axios';
import { toastify } from '../utils/toaster';

export function login_success(data) {
    return {
        type: actionTypes.LOGIN_SUCCESS,
        payload: data
    }
}

export function login_error(error) {
    return {
        type: actionTypes.LOGIN_ERROR,
        payload: error
    }
}

export function logout_success(data) {
    return {
        type: actionTypes.LOGOUT_SUCCESS,
        payload: data
    }
}

export function login(email, password) {
    return (dispatch) => {
        return axios.post('http://localhost:8000/api/login',
            {
                email: email,
                password: password,
                headers: {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": "*"
                }
            })
            .then(function (response) {
                toastify('success', 'Signing In Successful. Welcome To MyBlog !!');
                localStorage.setItem('token', response.data.token);
                dispatch(login_success(response.data.data));
                history.push('/home');
            })
            .catch(function (error) {
                toastify('error', 'Signing In Failed !! ' + error.response.data.message);
                dispatch(login_error(error.response.data.errors));
                history.push('/signin');
            })
    }
}

export function logout() {
    return (dispatch) => {
        var token = localStorage.getItem("token");
        return axios.get('http://localhost:8000/api/logout?token=' + token,
            {
                headers: {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": "*"
                }
            })
            .then(function (response) {
                if (response.status === 200) {
                    toastify('success', 'Logged Out Successfully !!');
                    localStorage.removeItem('token');
                    dispatch(logout_success(response.data.data));
                    history.push('/signin');
                }
            })
            .catch(function (error) {
                toastify('error', 'Failed !! Please Try Again');
                localStorage.removeItem('token');
                console.log(error);
            })
    }
}