import React from 'react';
import { Link } from 'react-router-dom';
import Helmet from 'react-helmet';
import { Container, Row, Col, Form, FormGroup, Label, Input, Button } from 'reactstrap';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { register }  from '../../actions/registerActions';

class Signup extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            name: '',
            email: '',
            password: '',
            error:{}
        }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if (JSON.stringify(nextProps.reg.error) !== JSON.stringify(prevState.error)) {
            return {
                error: nextProps.reg.error
            };
        }
        return null;
    }

    changeHandler = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    submitHandler = (event) => {
        event.preventDefault()
        this.props.actions.register(this.state.name, this.state.email, this.state.password);
    }

    render() {
        let { name, email, password, error } = this.state
        return (
            <div>
                <Helmet>
                    <title>Sign In | MyBlog</title>
                    <meta name="description" content="Sign In | Myblog" />
                </Helmet>
                <div className='signin'>
                    <Container>
                        <Row>
                            <Col sm="12" md={{ size: 6, offset: 3 }}>
                                <Form className="signin-form" onSubmit={this.submitHandler}>
                                    <div className="form-heading">
                                        <h2>Sign Up @ MyBlog</h2>
                                        <div className="st-border"></div>
                                    </div>
                                    <FormGroup>
                                        <Label>Your Name</Label>
                                        <Input
                                            type="text"
                                            name="name"
                                            id="name"
                                            className={
                                                error.name ? "is-invalid" : ""
                                            }
                                            value={name}
                                            placeholder="Type Your Name Here"
                                            onChange={this.changeHandler}
                                        />
                                        {error.name ? <div className="invalid-feedback">{error.name}</div> : ''}
                                    </FormGroup>
                                    <FormGroup>
                                        <Label>Your Email</Label>
                                        <Input
                                            type="email"
                                            name="email"
                                            id="email"
                                            className={
                                                error.email ? "is-invalid" : ""
                                            }
                                            value={email}
                                            placeholder="Type Your Email Here"
                                            onChange={this.changeHandler}
                                        />
                                        {error.email ? <div className="invalid-feedback">{error.email}</div> : ''}
                                    </FormGroup>
                                    <FormGroup>
                                        <Label>Your Password</Label>
                                        <Input
                                            type="password"
                                            name="password"
                                            id="password"
                                            className={
                                                error.password ? "is-invalid" : ""
                                            }
                                            value={password}
                                            placeholder="Type Your Password Here"
                                            onChange={this.changeHandler}
                                        />
                                        {error.password ? <div className="invalid-feedback">{error.password}</div> : ''}
                                    </FormGroup>
                                    <FormGroup>
                                        <Link className="form-link" to="/signin">Already registered? Sign In</Link>

                                    </FormGroup>
                                    <FormGroup>
                                        <Button
                                            type="submit"
                                            color="success"
                                            className="btn-block"
                                        >
                                            Register
                                        </Button>
                                    </FormGroup>
                                </Form>
                            </Col>
                        </Row>
                    </Container>
                </div>
            </div>
        )
    }
}


function mapStateToProps(state) {
    return {
        reg: state.reg
    }
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators({ register }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Signup);