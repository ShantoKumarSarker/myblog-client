import React from 'react';
import Helmet from 'react-helmet';
import Header from './../header/navigation';
import Footer from './../footer/footer';
import CreatePostForm from './../post/create';

export default class CreatePost extends React.Component {
    render() {
        return (
            <div>
                <Helmet>
                    <title>Create Post | MyBlog</title>
                    <meta name="description" content="Create Post | MyBlog" />
                </Helmet>
                <div>
                    <Header />
                    <CreatePostForm />
                    <Footer />
                </div>
            </div>
        )
    }
}