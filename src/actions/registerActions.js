import * as actionTypes from './actionTypes';
import axios from 'axios';
import { toastify } from '../utils/toaster';
import { history } from './../store';

export function register_success(data) {
    return {
        type: actionTypes.REGISTER_SUCCESS,
        payload: data
    }
}

export function register_error(error) {
    return {
        type: actionTypes.REGISTER_ERROR,
        payload: error
    }
}

export function register(name, email, password) {
    return (dispatch) => {
        return axios.post('http://localhost:8000/api/register',
            {
                name: name,
                email: email,
                password: password,
                headers: {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": "*"
                }
            }
        )
        .then(function (response) {
            toastify('success', 'Registration Successful !! Please Login');
            dispatch(register_success(response.data.data));
            history.push('/signin');
        })
        .catch(function (error) {
            toastify('error', 'Registration Failed !! ' + error.response.data.message);
            dispatch(register_error(error.response.data.errors));
            history.push('/signup');
        })
    }
}