import * as actionTypes from './actionTypes';
import axios from 'axios';
import { toastify } from '../utils/toaster';
import { history } from './../store';
import { broadcast } from '../actions/notificationActions';

export function getPostList(data) {
    return {
        type: actionTypes.FETCH_POST,
        payload: data
    }
}

export function createPostListPending() {
    return {
        type: actionTypes.CREATE_POST_PENDING
    }
}

export function createPostListError(error) {
    return {
        type: actionTypes.CREATE_POST_ERROR,
        payload: error
    }
}

export function createPostListSuccess() {
    return {
        type: actionTypes.CREATE_POST_SUCCESS
    }
}

export function updatePostListError(error) {
    return {
        type: actionTypes.UPDATE_POST_ERROR,
        payload: error
    }
}

export function updatePostListPending() {
    return {
        type: actionTypes.UPDATE_POST_PENDING
    }
}

export function updatePostListSuccess() {
    return {
        type: actionTypes.UPDATE_POST_SUCCESS
    }
}

export function deletePostListPending() {
    return {
        type: actionTypes.DELETE_POST_PENDING
    }
}

export function getlist() {
    return (dispatch) => {
        var token = localStorage.getItem("token");
        return axios.get('http://localhost:8000/api/posts?token=' + token,
            {
                headers: {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": "*"
                }
            })
            .then(function (response) {
                dispatch(getPostList(response.data.data));
            })
            .catch(function (error) {
                console.log(error);
            })
    }
}

export function createlist(title, description) {
    return (dispatch) => {
        var token = localStorage.getItem("token");
        return axios.post('http://localhost:8000/api/posts?token=' + token,
            {
                title: title,
                description: description,
                headers: {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": "*"
                }
            })
            .then(function () {
                dispatch(createPostListPending());
                toastify('success', 'Article Posted Successfully !!');
                return axios.get('http://localhost:8000/api/posts?token=' + token,
                    {
                        headers: {
                            "Content-Type": "application/json",
                            "Access-Control-Allow-Origin": "*"
                        }
                    })
                    .then(function (response) {
                        dispatch(getPostList(response.data.data));
                        dispatch(createPostListSuccess());
                        broadcast("New article posted");
                        history.push('/home');
                    })
                    .catch(function (error) {
                        console.log(error);
                    })
            })
            .catch(function (error) {
                toastify('error', 'Failed !! Please Try Again With Valid Information !!');
                dispatch(createPostListError(error.response.data.message));
            })
    }
}

export function updatelist(id, title, description) {
    return (dispatch) => {
        var token = localStorage.getItem("token");
        return axios.put('http://localhost:8000/api/posts/' + id + '?token=' + token,
            {
                title: title,
                description: description,
                headers: {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": "*"
                }
            })
            .then(function () {
                dispatch(updatePostListPending());
                toastify('success', 'Article Updated Successfully !!');
                return axios.get('http://localhost:8000/api/posts?token=' + token,
                    {
                        headers: {
                            "Content-Type": "application/json",
                            "Access-Control-Allow-Origin": "*"
                        }
                    })
                    .then(function (response) {
                        dispatch(getPostList(response.data.data));
                        dispatch(updatePostListSuccess());
                        history.push('/home');
                    })
                    .catch(function (error) {
                        console.log(error);
                    })
            })
            .catch(function (error) {
                toastify('error', 'Failed !! Please Try Again With Valid Information !!');
                dispatch(updatePostListError(error.response.data.message));
            })
    }
}

export function deletelist(id) {
    return (dispatch) => {
        var token = localStorage.getItem("token");
        return axios.delete('http://localhost:8000/api/posts/' + id + '?token=' + token,
            {
                headers: {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": "*"
                }
            })
            .then(function (response) {
                dispatch(deletePostListPending());
                toastify('success', 'Article Deleted Successfully !!');
                return axios.get('http://localhost:8000/api/posts?token=' + token,
                    {
                        headers: {
                            "Content-Type": "application/json",
                            "Access-Control-Allow-Origin": "*"
                        }
                    })
                    .then(function (response) {
                        dispatch(getPostList(response.data.data));
                        dispatch(createPostListSuccess());
                        broadcast("New article posted");
                        history.push('/home');
                    })
                    .catch(function (error) {
                        console.log(error);
                    })
            })
            .catch(function (error) {
                toastify('error', 'Failed !! Please Try Again');
                console.log(error);
            })
    }
}