import { combineReducers } from "redux"
import { routerReducer } from 'react-router-redux'
import loginReducer from './loginReducer'
import registerReducer from './registerReducer'
import postReducer from './postReducer'
import notificationReducer from './notificationReducer'

const rootReducer = combineReducers({
    router: routerReducer,
    auth : loginReducer,
    reg : registerReducer,
    post : postReducer,
    notification : notificationReducer
})

export default rootReducer
