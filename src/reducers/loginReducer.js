import * as types from '../actions/actionTypes';

const initialState = {
    token: {},
    loading: true,
    loggedIn: false,
    error: {}
};

export default function loginReducer(state = initialState, action) {
    switch (action.type) {
        case types.LOGIN_ERROR:
            return { ...state, loading: false, loggedIn: false, token: {}, error: action.payload };
        case types.LOGIN_SUCCESS:
            return { ...state, loading: false, loggedIn: true, token: action.payload, error: {} };
        case types.LOGOUT_SUCCESS:
            return { ...state, token: {}, error: {}, loggedIn: false };
        default:
            return state;
    }
}
