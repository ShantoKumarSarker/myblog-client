import React from 'react';
import { Container, Row, Col } from 'reactstrap';

export default class Footer extends React.Component {
    render() {
        return (
            <footer className="footer my-5">
                <Container>
                    <Row className="justify-content-between">
                        <Col sm="12" md="12">
                            <div className="text-center">
                            <p className="my-3">Copyright © 2019, All Rights Reserved.</p>
                            </div>
                        </Col>
                    </Row>
                </Container>
            </footer>
        )
    }
}