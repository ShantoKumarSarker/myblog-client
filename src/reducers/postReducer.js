import * as types from '../actions/actionTypes';

const initialState = {
    data: {
        data: {}
    },
    loading: true,
    error: {}
};

export default function postReducer(state = initialState, action) {
    switch (action.type) {
        case types.FETCH_POST:
            return { ...state, loading: false, data: action.payload, error: {} };
        case types.CREATE_POST_PENDING:
            return { ...state, loading: true };
        case types.CREATE_POST_ERROR:
            return { ...state, loading: false, error: action.payload };
        case types.CREATE_POST_SUCCESS:
            return { ...state, loading: false, error: {} };
        case types.UPDATE_POST_PENDING:
            return { ...state, loading: true };
        case types.UPDATE_POST_ERROR:
            return { ...state, loading: false, error: action.payload };
        case types.UPDATE_POST_SUCCESS:
            return { ...state, loading: false, error: {} };
        case types.DELETE_POST_PENDING:
            return { ...state, loading: true };
        default:
            return state;
    }
}
