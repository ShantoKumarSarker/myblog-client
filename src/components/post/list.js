import React from 'react';
import { Link } from 'react-router-dom';
import { Card, CardBody, Button } from 'reactstrap';
import Spinner from './../spinner/spinner';

export default class PostList extends React.Component {
    render() {
        if (this.props.data.loading) {
            return <div>
                <Spinner />
            </div>
        }

        return (
            <div>
                {this.props.data.data.map((article, index) => {
                    return (
                        <Card className="post-card" key={index}>
                            <CardBody>
                                <h5 className="card-title"> {article.title} </h5>
                                <span className="float-right">
                                    <Link to={"/edit-post/" + article.id} className="btn btn-primary mr-3"><i className="fas fa-pen-square"></i> Edit</Link>
                                    <Button color="danger" onClick={ () => this.props.onClickDeleteHandler(article.id)}><i className="fas fa-trash-alt"></i> Delete</Button>
                                </span>
                                <p className="card-text">
                                    <strong className="mr-3">
                                        Posted On: <span className="text-primary">{article.created_at}</span>
                                    </strong>
                                    <strong>
                                        Author: <span className="text-primary">{article.user}</span>
                                    </strong>
                                </p>
                                <p className="card-text">{article.description}</p>
                            </CardBody>
                        </Card>
                    )
                })}
            </div>
        )
    }
}