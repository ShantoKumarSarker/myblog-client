import { createStore, compose, applyMiddleware } from "redux";
import thunk from 'redux-thunk';
import { routerMiddleware } from 'react-router-redux';

import rootReducer from "./reducers";

import createHistory from "history/createBrowserHistory";

export const history = createHistory();

const middleware = routerMiddleware(history)

export const store =
    process.env.NODE_ENV === 'development' && window.__REDUX_DEVTOOLS_EXTENSION__
        ?
        createStore(
            rootReducer,
            compose(
                applyMiddleware(thunk, middleware),
                window.__REDUX_DEVTOOLS_EXTENSION__ &&
                window.__REDUX_DEVTOOLS_EXTENSION__()
            )
        )
        : createStore(
            rootReducer,
            compose(
                applyMiddleware(thunk, middleware)
            )
        );
