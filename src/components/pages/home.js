import React from 'react';
import Helmet from 'react-helmet';
import Header from './../header/navigation';
import Footer from './../footer/footer';
import PostList from './../post/list';
import { Container, Row, Col } from 'reactstrap';
import Spinner from './../spinner/spinner';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getlist, deletelist } from '../../actions/postActions';

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            data: [],
            error: {}
        }
    }

    componentDidMount() {
        this.props.actions.getlist();
    }

    onClickDelete = (id) => {
        this.props.actions.deletelist(id);
    }

    render() {
        if (this.props.post.loading) {
            return (
                <div>
                    <Spinner />
                </div>
            )
        } else {
            return (
                <div>
                    <Helmet>
                        <title>Home | MyBlog</title>
                        <meta name="description" content="Home | MyBlog" />
                    </Helmet>
                    <div>
                        <Header />
                        <div className="posts-wall">
                            <Container>
                                <Row>
                                    <Col sm="12" md="12">
                                        <div className="posts-wall-heading">
                                            <h2>All Post</h2>
                                            <div className="st-border"></div>
                                        </div>
                                    </Col>
                                    <Col sm="12" md="12">
                                        <PostList data={this.props.post} onClickDeleteHandler={this.onClickDelete.bind(this)} />
                                    </Col>
                                </Row>
                            </Container>
                        </div>
                        <Footer />
                    </div>
                </div>
            )
        }
    }
}

function mapStateToProps(state) {
    return {
        post: state.post
    }
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators({ getlist, deletelist }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);